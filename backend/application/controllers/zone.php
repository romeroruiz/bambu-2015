<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zone extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('manager_model', 'manager');
		$this->load->model('proyectos_model', 'proyectos');
	}
	public function index()
	{
		$data = array(
			'edificios' => $this->proyectos->all(),
		);
		$this->load->view('zone', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */