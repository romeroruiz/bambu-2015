<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('manager_model', 'manager');
		$this->load->model('proyectos_model', 'proyectos');
	}
	public function index()
	{
		$data = array(
			'edificios' => $this->proyectos->all(),
		);
		$this->load->view('contact', $data);
	}

	public function send()
	{
		@date_default_timezone_set('America/Lima');

		$datos = $this->input->post();
		$post = (object)$datos;  
		$usuario = 'No Reply';
		$correo = 'raul@romeroruiz.com';

	    $message = "<b>Nombres : </b>".$post->fullname."<br>"
	              ."<b>Apellidos : </b>".$post->lastname."<br>"
	              ."<b>Correo : </b>".$post->email."<br>"
	              ."<b>Teléfono : </b>".$post->phone."<br>"
	              ."<b>Asunto : </b>".$post->subject."<br>"
	              ."<b>Mensaje : </b><br>"
	              .$post->message;

        $params = array(
        	'from' => array($post->email, utf8_decode($post->fullname." ".$post->lastname)),
        	'subject' => utf8_decode("Contactenos Bambú"),
        	'message' => utf8_decode($message),
        	'address' => array($correo, utf8_decode($usuario)),
        );

		$this->load->library('php_mailer');
		$send = $this->php_mailer->send($params);
		$datos['fecha'] = date("Y-m-d H:i:s");
		$datos['send'] = 0;
    	$response = array(
        	"result" => "error",
        	"message" => "Se produjo un error, intentelo más tarde",
        );
		if ($send) {
            $response = array(
	        	"result" => "success",
	        	"message" => "Pronto te atenderemos y daremos una respuesta al Correo Electrónico que has registrado.",
	        );
	        $datos['send'] = 1;
        }

	    @$this->manager->insert('contactos', $datos);

	    echo json_encode($response);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */