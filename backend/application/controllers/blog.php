<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('manager_model', 'manager');
		$this->load->model('noticias_model', 'noticias');
		$this->load->model('proyectos_model', 'proyectos');
	}
	public function index($url="")
	{
		$data = array(
			'news' => $this->get_news(),
			'edificios' => $this->proyectos->all(),
		);
		$this->load->view('blog', $data);
	}

	public function more() {
		$limit = 6;
		$start = $this->input->post('start');
		$news = $this->get_news($start, $limit);
		$response = array(
			'news' => $news,
			'total' => count($this->noticias->all())
		);
		echo json_encode($response);
	}

	public function article($url="")
	{
		$uri = explode("-", $url);
		$data = array(
			'article' => $this->noticias->get_noticia($uri[count($uri)-1]),
			'edificios' => $this->proyectos->all(),
		);
		$this->load->view('article', $data);
	}


	public function get_news($start=0, $limit=6)
	{
		return $this->noticias->get_news($start, $limit);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */