<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Builds extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('manager_model', 'manager');
		$this->load->model('proyectos_model', 'proyectos');
	}

	public function index()
	{
		$data = array(
			'proyectos' => $this->proyectos->get_proyectos(),
			'edificios' => $this->proyectos->all(),
		);
		$this->load->view('builds', $data);
	}

	public function detail($url)
	{
		$id = $this->proyectos->get_idbyurl($url);
		$data = array(
			'edificio' => $this->proyectos->get_proyecto($id), 
			'banners'  => $this->proyectos->get_banners($id), 
			'offices' => $this->proyectos->get_offices($id), 
			'edificios' => $this->proyectos->all(), 
		);
		$this->load->view('build', $data);
	}

	public function geolocations()
	{
		$proyect = $this->input->post('proyect');
		//echo json_encode($this->proyectos->get_geolocations($proyect));
		//exit;
		$id = ($proyect=='all') ? '':$this->proyectos->get_idbyurl($proyect);
		$data = array(
			'build' => $this->proyectos->get_geolocations($proyect), 
			'equipment' => $this->proyectos->get_equipment($id), 
		);
		echo json_encode($data);
	}

	public function offices()
	{
		$proyect = $this->input->post('proyect');
		$id = $this->proyectos->get_idbyurl($proyect);
		echo json_encode($this->proyectos->get_offices($id));
	}

	public function detail_office($id) 
	{
		echo json_encode($this->proyectos->get_detailoffice($id));
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */