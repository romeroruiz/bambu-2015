<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PHP_Mailer {

    public function PHP_Mailer() {
        require_once('PHPMailer/class.phpmailer.php');
    }

    public function send($params) {
        require_once('PHPMailer/class.phpmailer.php');
            
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = TRUE; // enabled SMTP authentication
        //$mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "mail.consumemejor.com";      // setting GMail as our SMTP server
        $mail->Port       = 25;                   // SMTP port to connect to GMail
        $mail->Username   = "sender@consumemejor.com";  // user email address
        $mail->Password   = "UE?0n59W.4WL";            // password in GMail
        $mail->SetFrom( $params['from'][0], $params['from'][1]);  //Who is sending the email
        $mail->AddReplyTo( $params['from'][0], $params['from'][1] );  //email address that receives the response
        $mail->Subject    = $params['subject'];
        $mail->Body       = $params['message'];
        $mail->IsHTML(true);
        $mail->AddAddress( $params['address'][0], $params['address'][1] );
        if (isset($params['attach'])) {
            $mail->AddAttachment($params['attach']['uploadFile']['tmp_name'],
                         $params['attach']['uploadFile']['name']);
        }

        if($mail->Send()) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
}