  /*
* All configurations for frontend must be here, 
* do not modify any setup from grunt's task
* Remenber this skeleton support integration with most framework
*/
var config = {
  metas : {
    title: 'Bambu | Inmuebles y Oficinas',
    description: '',
    keywords: '',
    analytics_ua: '',
    facebook_id: ''
  },
  template_ext: function () {
    var ext;
    if (this.env === 'prod') {
      return '.php';
    }
    return '.html';
  },
  static_uri: function () {
    /* Método para cargar los archivos státicos CSS, JS, Images */
    if (this.env === "prod") {
      return '/site';
    }
    return '/static';    
  },
  deploy_routes : function () {
    var routes = {},
        base,
        static_path;
    if (this.env === 'prod') {
      base = '../../backend/';
      static_path =  base + 'static/';
      routes = {
        base: 'static',
        templates: base + 'application/views/',
        static: static_path,
        styles: static_path + '/styles/',
        images: static_path + '/images/',
        fonts: static_path + '/fonts/',
        scripts: static_path + '/scripts/',
        sprites: static_path + '/sprites/'
      }
    } else {
      base = 'build';
      static_path =  base + '/static';
      routes = {
        base : base,
        templates: base + '/templates',
        static: static_path,
        styles: static_path + '/styles',
        images: static_path + '/images',
        fonts: static_path + '/fonts',
        scripts: static_path + '/scripts',
        sprites: static_path + '/sprites'
      }
    }
    return routes;
  },
  template_uri: function(name) {
    return this.urls[name].get('uri');
  },
  reverse_url: function(name) {
    if(this.type == 'prod'){
      return '<?php echo $this->url(' + name + ');?>'
    }
    return this.urls[name].get('url');
  },
  static_url: function(url) {
    if (this.env == 'prod') {
      return '<?php echo base_url().\'static/' + url + '\';?>'
      //return '<?php echo base_url();?>' + url  
    }
    return this.static_uri() + '/' + url;
  },
  setEnv : function (env) {
    this.env = env;
  },
  getEnv: function () {
    return this.env;
  },
  urls: {},
  set_url: function(name, obj) {
    this.urls[name] = new this.Url(obj);
  },
  Url: function(obj) {
    this.obj = obj;
    this.get = function(name) {
      return this.obj[name];
    };
    return this;
  }
};

module.exports = config;