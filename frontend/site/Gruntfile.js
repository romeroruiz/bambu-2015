
module.exports = function(grunt) {
  try {
    config = require('./config.js');
    if (grunt.option('prod')) {
      config.setEnv('prod');
    }
    else {
      config.setEnv('dev');
    }
  }
  catch (e) {
    
    config = {};
  }
  grunt.config.set('config', config);
  grunt.loadTasks('./tasks');
};

