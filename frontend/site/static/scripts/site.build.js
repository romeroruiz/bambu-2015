require([
  'jquery',
  'slick',
  'ddslick',
  'scrolltop',
  'menu-responsive',
  'validate',
  'validate-subscribe',
  'async'
  ], function (jquery, slick, ddslick, scrollTop, menuResponsive, validate, validateSubscribe, async) {

    var $doc = $(document);

    function slickSlideBuild() {
      $('.Build-Slider').slick({
        speed: 500
      });
    }

    function customSelectEquiptment() {
      ddData = [
        {
          text: "Equipamiento Urbano",
          value: 0,
          selected: true
        },
        {
          text: "Restaurantes",
          value: 1,
          selected: false,
          imageSrc: "/static/images/select/select-restaurantes.png"
        },
        {
          text: "Bancos",
          value: 2,
          selected: false,
          imageSrc: "/static/images/select/select/select-bancos.png"
        },
        {
          text: "Supermercados",
          value: 3,
          selected: false,
          imageSrc: "/static/images/select/select-supermercado.png"
        },
        {
          text: "Transporte",
          value: 4,
          selected: false,
          imageSrc: "/static/images/select/select-transport.png"
        },
        {
          text: "Ver Todos",
          value: "all",
          selected: false,
          imageSrc: "/static/images/select/select-todos.png"
        }
      ];

      $('#officesEquipment').ddslick({
        data: ddData,
        width: 300,
        imagePosition: "left",
        selectText: "Equipamiento Urbano",
        onSelected: function(data){
          //console.log(data);
          type = data.selectedData.value;
          for (var x in markers) {
            markers[x].setMap(map);
            if (markers[x].tipo!=type && type!="all") {
              markers[x].setMap(null);
            }
          }
        }
      });

      $('.dd-option').on('click', function(){
        $('.dd-select').css('padding', '0');
      });
    }

    var map,
        markers = [];
    var iconBase = '/static/images/mapmaker/';
    var icons = ['', 'ico-food.png', 'ico-shop.png', 'ico-shop.png', 'ico-bus.png'];
    function initializeMaps(_location) {
      mapa = _location.build.map.split(",");
      var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(mapa[0], mapa[1])
      };

      map = new google.maps.Map(document.getElementById('maps'),
        mapOptions);

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(mapa[0], mapa[1]),
          map: map,
          title: _location.build.proyect,
          icon: iconBase + "marker-zone-contact.png",
        });

      google.maps.event.addListener(marker, 'click', function(){
        map.setZoom(8);
        map.setCenter(marker.getPosition());
      });

      for(i=0; i<_location.equipment.length; i++) {
        mapa = _location.equipment[i].mapa.split(",");
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(mapa[0], mapa[1]),
          map: map,
          title: _location.equipment[i].establecimiento,
          tipo: _location.equipment[i].id_tipo,
          icon: iconBase + icons[_location.equipment[i].id_tipo],
        });
        markers.push(marker);
      }
    }

    function initializeOffices(_response) {
      oficinas = _response;
      _html = "<option =''>Seleccionar</option>";
      for (i=0; i<_response.length; i++) {
        _html +="<option value='"+_response[i].id+"'>"+_response[i].nombre+"</option>";
      }
      $('#offices').html(_html);

      //price = (_response[0].precio>0) ? "S/. "+_response[0].precio:"No Disponible";
      area  = (_response[0].area  >0) ? _response[0].area+"m<sup>2</sup>":"No Disponible";
      $('#price').find('span').text(oficinas[0].precio);
      //$('#price').find('img').attr('src', "/"+oficinas[0].imagen3d);
      //loadImage("/"+oficinas[0].imagen3d, '#price > img');
      _images.push("/"+oficinas[0].imagen3d);
      _containers.push('#price > img');
      $('#area').find('span').html(area);
      //$('#area').find('img').attr('src', "/"+oficinas[0].plano);
      //loadImage("/"+oficinas[0].plano, '#area > img');
      _images.push("/"+oficinas[0].plano);
      _containers.push('#area > img');
      //console.log([_images, _containers]);
      loadImage(_images[0], _containers[0]);
    }

    function init() {
      slickSlideBuild();
      scrollTop( $('.Button-Top') );
      //customSelectOffice();
      customSelectEquiptment();
      menuResponsive($('.Menu-Sandwich'));
      validateSubscribe('.Suscribe-Form');
      uri = location.pathname.split('/');
      $.post('/geolocations', {proyect: uri[uri.length-1] }, function(response, textStatus, xhr) {
        initializeMaps(response);
      }, "json");
      $.post('/offices', {proyect: uri[uri.length-1] }, function(response, textStatus, xhr) {
        //console.log(response);
        initializeOffices(response);
      }, "json");
      $('#offices').change(function(event) {
        _images = [],
        _containers = [],
        _c = 0;

        id = $(event.target).val();
        index = event.target.selectedIndex - 1;
        if (index<0)
          return;

        area  = (oficinas[index].area  > 0) ? oficinas[index].area+"m<sup>2</sup>":oficinas[index].area;
        $('#price').find('span').text(oficinas[index].precio);
        $('#price').find('img').attr('src', "/"+oficinas[index].imagen3d);
        _images.push("/"+oficinas[index].imagen3d);
        _containers.push('#price > img');
        $('#area').find('span').html(area);
        //$('#area').find('img').attr('src', "/"+oficinas[index].plano);
        _images.push("/"+oficinas[index].plano);
        _containers.push('#area > img');
        loadImage(_images[0], _containers[0]);
        return;
        $.post('/offices/'+id, {}, function(response, textStatus, xhr) {
          //console.log(response);
          area = (!isNaN(response.area)) ? response.area+"m<sup>2</sup>":response.area;
          $('#price').find('span').text(response.precio);
          $('#price').find('img').attr('src', "/"+response.imagen3d);
          $('#area').find('span').html(area);
          $('#area').find('img').attr('src', "/"+response.plano);
        }, "json");
      });
    }

    var _images = [],
        _containers = [],
        _c = 0;

    function loadImage(_src, _container) {
      loading();
      image = new Image();
      $(_container).attr('src', null);
      if (_c==_images.length) {
        console.log('End');
        return;
      }
      _c++;
      image.onload = function(e){
        $(_container).attr('src', image.src);
        loadImage(_images[_c], _containers[_c]);
        complete();
      };
      image.onerror = function(e) {
        $(_container).attr('src', "/static/images/Edificio_thumb_390x400.jpg");
      };
      image.onabort = function(e) {
        $(_container).attr('src', "/static/images/Edificio_thumb_390x400.jpg");
      };
      image.src = _src;
    }

    function loading() {
      console.log('Loading...');
    }

    function complete() {
      console.log('complete...');
    }

    $doc.ready(init);
});
