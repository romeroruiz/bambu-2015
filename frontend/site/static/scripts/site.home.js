require([
  'jquery',
  'slick',
  'validate',
  'validate-subscribe',
  'scrolltop',
  'menu-responsive',
  'fancybox'
	], function (jquery, slick, validate, validateSubscribe, scrollTop, menuResponsive, fancybox) {

    var $doc = $(document);

    function slickSlideHome() {
      $('.Home-Slider').slick({
        speed: 500
      });
    }

    function changeHeader() {

      var $topHeader = $('.Top-Header'),
          $logoText = $('.Logo-Text'),
          $menuList = $('.menu ul'),
          $bigLogo = $('.Logo');

      $(function(){
        $topHeader.data('size', 'big');
      });

      $(window).scroll(function(){
        if($(document).scrollTop() > 0)
        {
          if($topHeader.data('size') == 'big')
          {
            $topHeader.data('size', 'small');
            $logoText.addClass('hide-logo');
            $bigLogo.addClass('hide-biglogo');
            $menuList.stop().animate({
              top: '0'
            });
            $topHeader.addClass('Top-Scroll');
          }
        } else {
          if($topHeader.data('size') == 'small')
          {
            $topHeader.data('size', 'big');
            $logoText.removeClass('hide-logo');
            $bigLogo.removeClass('hide-biglogo');
            $menuList.stop().animate({
              top: '15px'
            });
            $topHeader.removeClass('Top-Scroll');
          }
        }
      });
    }

    function init() {
      scrollTop( $('.Button-Top') );
      validateSubscribe('.Suscribe-Form');
      slickSlideHome();
      changeHeader();
      menuResponsive( $('.Menu-Sandwich') );
    }

		$doc.ready(init);

});
