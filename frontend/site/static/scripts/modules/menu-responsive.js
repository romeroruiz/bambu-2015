define([
  'jquery'
], function($) {
  
  function menuResponsive($menu) {

    $menu.on( 'click', menuTop );

    function menuTop(event) {
      event.preventDefault();
      console.log('apears menu');
      $('.Menu-Default').toggleClass('show-menu');
    }

  }
  
  return menuResponsive;

})