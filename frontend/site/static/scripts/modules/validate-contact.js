define(['validate', 'additional-methods'], function () {

    function validateContact($form){
      
      $('.Contact-Form').validate({
        rules: {
          fullname: {
            required: true,
            lettersonly: true
          },
          lastname: {
            required: true,
            lettersonly: true
          },
          email: {
            required: true, 
            email: true,
            validemail: true
          },
          phone: {
            required: true, 
            digits: true
          },
          subject: {
            required: true, 
            alphanumeric: false
          },
          message: {
            required: true, 
            minlength: 10,
            alphanumeric: false
          }
        },
        messages: {
          fullname: {
            required: 'El campo nombres no puede estar vacío'
          },
          lastname: {
            required: 'El campo apellidos no puede estar vacío'
          },
          email: {
            required: 'El campo de correo electrónico no puede estar vacío', 
            email: 'Correo inválido',
            validemail : 'Escriba un correo válido'
          },
          phone: {
            required: 'El campo télefono no puede estar vacío', 
            digits: 'Debe ser número'
          },
          subject: {
            required: 'El campo asunto no puede estar vacío', 
            alphanumeric: 'Debe ser asunto correcto'
          },
          message: {
            required: 'El campo mensaje no puede estar vacío', 
            minlength: '¡Es muy poco texto!',
            alphanumeric: 'Debe ser mensaje correcto'
          }
        },
        submitHandler: function(form) {
          $.fancybox.open('#Contact-Content');
          console.log('recompile and validate');
          //form.submit();
          $fancybox(".Thanks-Content").html();
          
        }
      });
      
      $.validator.addMethod("validemail", function(value, element) {
        return this.optional(element) || /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i 
      }, "Por favor escriba un correo electrónico correcto");

      $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]*$/i.test(value);
      }, "Letras unicamente por favor"); 

      
      $('.Button-Close').on( 'click' , function() {
        parent.$.fancybox.close()
        console.log('click');
      });

      $('.Btn-Contact').on( 'click' , function(e) {
        e.preventDefault();
        if ($('.Contact-Form').valid()) {
          console.log('Sending form');
          $.post($('.Contact-Form').attr('action'), $('.Contact-Form').serialize(), function(response, textStatus, xhr) {
            //console.log(response.message);
            TitleMessage = (response.result=="success") ? "Mensaje Enviado":"Mensaje no Enviado";
            $('#Contact-Content').find('h4').text(TitleMessage);
            $('#Contact-Content').find('p').text(response.message);
            $('#Contact-Content').fadeIn(500);
          }, "json");
        }
      });

      $('.Button-Continue').on('click', function(event) {
        event.preventDefault();
        document.forms[0].reset();
        $('#Contact-Content').fadeOut(500);
      });
    }

    return validateContact;

	}
)