define(['validate', 'fancybox'], function () {

    function validateSubscribe($form){

      $('.Suscribe-Form').validate({
        rules: {
          emailSuscribe: {
            required: true,
            email: true, 
            minlength: 6
          }
        },
        messages: {
          emailSuscribe: {
            required: "El campo no debe estar vacio.",
            email: "Email incorrecto",
            minlength: "No tiene los caracteres minimos"
          }
        },
        submitHandler: function(form) {
          console.log('tell that validate');
          //form.submit();
          $.fancybox.open('#Thanks-Content');
          //$fancybox(".Thanks-Content").html();
        }
      });
      
      $('.Button-Close').on( 'click' , function() {
        parent.$.fancybox.close()
        console.log('click');
      });

    }
    return validateSubscribe;
  }
)