define(['validate'], function () {

  function setupCustomDisplayMessages () {
    if (jQuery.validator && jQuery.validator.setDefaults) {
      jQuery.validator.setDefaults({
        
        highlight: function(element, errorCls, validCls) {
          var el = $(element);
          var parent = el.parent();

          if (el.is('select, :file')) {
            parent.addClass(errorCls + '-custom').removeClass(validCls + '-custom');
          }

          else {
            el.addClass(errorCls).removeClass(validCls);
          }
        },
        unhighlight: function(element, errorCls, validCls) {
          var el = $(element);
          var parent = el.parent();


          if (el.is('select, :file')) {
            parent.removeClass(errorCls + '-custom').addClass(validCls + '-custom');
          }

          else {
            el.addClass(validCls).removeClass(errorCls);
          }
        }   
        
      });
    }
  };

  function setupRulesRegisterForm(form) {

    var $form = form;
    var $numeric = $('.numeric');

    setupCustomDisplayMessages();
    $numeric.numeric();
    $form.validate();

   $('#first_name').rules("add", {
      required: true,
      maxlength: 40,
      minlength: 2,
      messages: {
        required: "Ingresa tu Nombre",
        maxlength: "No ingreses más de 40 caracteres",
        minlength: "Ingresa como mínimo 2 caracteres"
      }
    });

    $('#last_name').rules("add", {
      required: true,
      maxlength: 40,
      minlength: 2,
      messages: {
        required: "Ingresa tus Apellidos",
        maxlength: "No ingreses más de 40 caracteres",
        minlength: "Ingresa como mínimo 2 caracteres"
      }
    });

    $("#dni").rules("add", {
        required: true,
        maxlength: 8,
        minlength: 8,
        messages: {
          required: "Ingresa tu DNI",
          maxlength: "El DNI debe tener 8 dígitos",
          minlength: "El DNI debe tener 8 dígitos"
        }
      });

    $("#phone").rules("add", {
        required: true,
        maxlength: 9,
        minlength: 7,
        messages: {
          required: "Ingresa tu Teléfono",
          maxlength: "No ingreses más de 9 dígitos",
          minlength: "No ingreses menos de 7 dígitos"
        }
      });

    $("#email").rules("add", {
      required: true,
      maxlength: 100,
      email: true,
      messages: {
        required: "Ingresa tu Email",
        email: "Ingresa un Email válido"
      }
    });

    $("#tyc").rules("add", {
      required: true,
      messages: {
        required: "Debes aceptar los términos y condiciones",
      }
    });

  };

   function loadProv() {
    var $this = $(this),
        selectedVal = $this.val(),
        $el = $('#cod_prov'),
        $sub_el = $('#cod_dist'),
        html = '<option value="">Selecciona</option>',
        item;

    if (selectedVal === '') {
      $el.html(html)
        .attr('disabled')
        .parent().addClass('ci-select-disabled')
    } else  {

      $el.html(html);
      $el.removeAttr('disabled');
      $el.parent().removeClass('ci-select-disabled');
      $el.trigger('change');
      $sub_el.html(html).attr('disabled','disabled');
      $sub_el.parent().addClass('ci-select-disabled');
      $sub_el.trigger('change');

      $.get($el.attr('data-source'), {
        'cod_dpto' : selectedVal
      }).done( function(data) {
        for (item in data) {
          html += '<option value="' + data[item].code + '">' + data[item].name + '</option>';
        }
        $el.html(html);
      });  
    }
  };

  function loadDist() {
    var $el = $('#cod_dist'),
        $dept = $('#cod_dpto'),
        $this = $(this),
        html = '<option value="">Selecciona</option>',
        selectedVal = $this.val(),
        item;

    if (selectedVal === '') {
      $el.html(html);
      $el.attr('disabled','disabled');
      $el.parent().addClass('ci-select-disabled');
      $el.trigger('change');
    } else {
      $el.removeAttr('disabled');
      $el.parent().removeClass('ci-select-disabled');
      $el.trigger('change');
      $.get($el.attr('data-source'), {
        'cod_prov' : $this.val(),
        'cod_dpto' : $dept.val(),
      }).done(function (data) {
        for (item in data) {
          html += '<option value="' + data[item].code + '">' + data[item].name + '</option>';
        }
        $el.html(html);
      });
    }
  };

  function parseErrors(data) {
    var keys_erros = data.message,
        error_msg = "",
        key, 
        selector = "";
    for (key in keys_erros) {
      selector_id = "#" + key.split('_')[0] + "_error";
      error_msg = keys_erros[key];
      $(selector_id)
        .text(error_msg)
        .show();
    }
  };

  function trackEventSubmitForm(category, action, label) {
     ga('send', 'event', category, action, label);
  };

  function setupRegisterForm(settings) {
    var $form = $(settings.form);
    setupRulesRegisterForm($form);
    $form.on('submit', { settings : settings}, onSubmitRegisterForm);
    dpt.on('change', changeValSelect);
    dpt.on('change', loadProv);
    prov.on('change', changeValSelect);
    prov.on('change', loadDist);
    dist.on('change', changeValSelect);
    dpt.val('15');
    dpt.trigger('change');
  };

  function onSubmitRegisterForm (e) {

    var $form = $(this),
        settings = e.data.settings,
        tracking = settings.tracking;

    if ($form.valid()) {
      if (settings.isAjax) {
        var payload = {};
        payload.first_name = $('#first_name').val();
        payload.last_name = $('#last_name').val();
        payload.email = $('#email').val();
        payload.dni = $('#dni').val();
        payload.phone = $('#phone').val();
        payload.cod_dpto = $('#cod_dpto').val();
        payload.cod_prov = $('#cod_prov').val();
        payload.cod_dist = $('#cod_dist').val();
        payload.tyc = $('#tyc').val();
        payload.csrf_token = $('#csrf_token').val();
        $.post($form.attr('action'), data)
          .done(function(data) {
            if (data.status_code != 0) {
              parseErrors(data);
            } else {
              trackEventSubmitForm(tracking.category, tracking.action, tracking.label);
            }
          })
          .fail(function() {

          });
      }
      if (tracking)  {
        trackEventSubmitForm(tracking.category, tracking.action, tracking.label);
      }
    } else {
      return false;
    }
  }

  return {
    setupRegisterForm : setupRegisterForm
  };

});