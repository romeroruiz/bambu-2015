require([
  'jquery',
  'validate',
  'additional-methods',
  'validate-subscribe',
  'validate-contact',
  'scrolltop',
  'menu-responsive',
  'fancybox',
  'async'
  ], function (jquery, validate, additional_methods, validateSubscribe, validateContact, scrollTop, menuResponsive, fancybox, async) {

    var $doc = $(document);

    function initializeMaps() {

      var mapOptions = {
        zoom: 17,
        center: new google.maps.LatLng(-12.098313, -77.013688)
      };

      var map = new google.maps.Map(document.getElementById('maps'),
        mapOptions);

      var iconBase = 'static/images/mapmaker/';
      var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        icon: iconBase + 'marker-zone-contact.png',
        title: 'Click to zoom'
      });

      google.maps.event.addListener(map, 'center_changed', function(){
        window.setTimeout(function() {
          map.panTo(marker.getPosition());
        }, 3000);
      });

      google.maps.event.addListener(marker, 'click', function(){
        map.setZoom(8);
        map.setCenter(marker.getPosition());
      });
    }

    function init() {
      initializeMaps();
      scrollTop( $('.Button-Top') );
      validateSubscribe('.Suscribe-Form');
      validateContact();
      menuResponsive( $('.Menu-Sandwich') );
    }

    $doc.ready(init);
});
