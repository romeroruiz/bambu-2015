require([
  'jquery',
  'slick',
  'scrolltop',
  'menu-responsive',
  'validate',
  'validate-subscribe',
  'async'
  ], function (jquery, slick, scrollTop, menuResponsive, validate, validateSubscribe, async) {

    var $doc = $(document);

    function slickSlideBuild() {
      $('.Build-Slider').slick({
        speed: 500
      });
    }

    function initializeMaps(_locations) {

      var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(-12.098313, -77.013688)
      };

      var map = new google.maps.Map(document.getElementById('maps'),
        mapOptions);
      var iconBase = 'static/images/mapmaker/';
      var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        icon: iconBase + 'marker-zone.png',
        title: 'Click to zoom'
      });

      for(i=0; i<_locations.build.length; i++) {
        mapa = _locations.build[i].map.split(",");
        var iconBase = 'static/images/mapmaker/';
        var icons = {
          bus: {
            icon: iconBase + 'icon-bus.png'
          },
          food: {
            icon: iconBase + 'icon-food.png'
          },
          shoop: {
            icon: iconBase + 'icon-shoop.png'
          }
        };

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(mapa[0], mapa[1]),
          map: map,
          title: _locations.build[i].proyect,
          icon: iconBase + "marker-zone-contact.png"
        });
      }

      google.maps.event.addListener(marker, 'click', function(){
        map.setZoom(8);
        map.setCenter(marker.getPosition());
      });
    }

    function init() {
      slickSlideBuild();
      menuResponsive($('.Menu-Sandwich'));
      scrollTop( $('.Button-Top') );
      validateSubscribe('.Suscribe-Form');

      $.post('/geolocations', {proyect:'all'}, function(response, textStatus, xhr) {
        initializeMaps(response);
      }, "json");
    }

    $doc.ready(init);
});
