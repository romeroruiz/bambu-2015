require([
  'jquery',
  'scrolltop',
  'menu-responsive',
  'validate',
  'validate-subscribe'
  ], function (jquery, scrollTop, menuResponsive, validate, validateSubscribe) {

    var $doc = $(document);

    function init() {
      scrollTop( $('.Button-Top') );
      menuResponsive($('.Menu-Sandwich'));
      validateSubscribe('.Suscribe-Form');
    }

    $doc.ready(init);
});
