require([
  'jquery',
  'slick',
  'scrolltop',
  'menu-responsive',
  'validate',
  'validate-subscribe'
  ], function (jquery, slick, scrollTop, menuResponsive, validate, validateSubscribe) {

    var $doc = $(document);

    function slickSlideZone() {
      $('.Zone-Slider').slick({
        speed: 500
      });
    }

    function initializeMaps() {

      var mapOptions = {
        zoom: 17,
        center: new google.maps.LatLng(-12.098313, -77.013688)
      };

      var map = new google.maps.Map(document.getElementById('maps'),
        mapOptions);
      var iconBase = 'static/images/mapmaker/';
      var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        icon: iconBase + 'marker-zone.png',
        title: 'Click to zoom'
      });

      google.maps.event.addListener(map, 'center_changed', function(){
        window.setTimeout(function() {
          map.panTo(marker.getPosition());
        }, 3000);
      });

      google.maps.event.addListener(marker, 'click', function(){
        console.log('Click!!');
        map.setZoom(15);
        map.setCenter(marker.getPosition());
      });

      $.post('/geolocations', {proyect:'all'}, function(response, textStatus, xhr) {
        for(i=0; i<response.build.length; i++) {
          mapa = response.build[i].map.split(",");
          var iconBase = 'static/images/mapmaker/';
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(mapa[0], mapa[1]),
            map: map,
            title: response.build[i].proyect,
            icon: iconBase + "marker-zone-contact.png"
          });
        }
      }, "json");
    }

    function manageSound() {

      var setAudio = {
        trackOne: '01_sala_directorio',
        trackTwo: '02_sala_bambu',
        trackThree: '03_zona_gourmet'
      };

      var firstAudio = document.getElementById(setAudio.trackOne),
          secondAudio = document.getElementById(setAudio.trackTwo),
          thirtyAudio = document.getElementById(setAudio.trackThree);

      var $btnPauseFirst = $('#directorio .Zone-Audio-Pause'),
          $btnPlayFirst = $('#directorio .Zone-Audio-Play'),
          $btnPauseSecond = $('#bambu .Zone-Audio-Pause'),
          $btnPlaySecond = $('#bambu .Zone-Audio-Play'),
          $btnPauseThree = $('#zona-gourmet .Zone-Audio-Pause'),
          $btnPlayThree = $('#zona-gourmet .Zone-Audio-Play'),
          $btnStop = $('.Zone-Audio-Stop');

      function stopAllSounds() {
        firstAudio.pause();
        firstAudio.currentTime = 0;
        secondAudio.pause();
        secondAudio.currentTime = 0;
        thirtyAudio.pause();
        thirtyAudio.currentTime = 0;
      }

      function playFirstAudio() {
        console.log(setAudio.trackOne);

        firstAudio.play();

        secondAudio.pause();
        secondAudio.currentTime = 0;
        thirtyAudio.pause();
        thirtyAudio.currentTime = 0;

        $btnPauseFirst.show();

        $btnPlaySecond.show();
        $btnPauseSecond.hide();
        $btnPlayThree.show();
        $btnPauseThree.hide();
      }

      function playSecondAudio() {
        console.log(setAudio.trackTwo);

        secondAudio.play();

        firstAudio.pause();
        firstAudio.currentTime = 0;
        thirtyAudio.pause();
        thirtyAudio.currentTime = 0;

        $btnPauseSecond.show();

        $btnPlayFirst.show();
        $btnPauseFirst.hide();
        $btnPlayThree.show();
        $btnPauseThree.hide();
      }

      function playThirdAudio() {
        console.log(setAudio.trackThree);

        thirtyAudio.play();

        firstAudio.pause();
        firstAudio.currentTime = 0;
        secondAudio.pause();
        secondAudio.currentTime = 0;

        $btnPauseThree.show();

        $btnPlayFirst.show();
        $btnPauseFirst.hide();
        $btnPlaySecond.show();
        $btnPauseSecond.hide();
      }

      $btnPlayFirst.on('click', function(){
        $(this).hide();
        playFirstAudio();
      });

      $btnPauseFirst.on('click', function() {
        firstAudio.pause();
        $(this).hide();
        $btnPlayFirst.show();
      });

      $btnPlaySecond.on('click', function(){
        $(this).hide();
        playSecondAudio();
      });

      $btnPauseSecond.on('click', function() {
        secondAudio.pause();
        $(this).hide();
        $btnPlaySecond.show();
      });

      $btnPlayThree.on('click', function(){
        $(this).hide();
        playThirdAudio();
      });

      $btnPauseThree.on('click', function() {
        thirtyAudio.pause();
        $(this).hide();
        $btnPlayThree.show();
      });

      $btnStop.on('click', function(){
        stopAllSounds();
        $('.Zone-Audio-Pause').hide();
        $('.Zone-Audio-Play').show();
      });
    }

    function init() {
      initializeMaps();
      slickSlideZone();
      scrollTop( $('.Button-Top') );
      validateSubscribe('.Suscribe-Form');
      manageSound();
      menuResponsive($('.Menu-Sandwich'));
    }

    $doc.ready(init);
});
