require([
  'jquery',
  'scrolltop',
  'menu-responsive',
  'validate',
  'validate-subscribe'
  ], function (jquery, scrollTop, menuResponsive, validate, validateSubscribe) {

    var $doc = $(document);

    TotalNews = 0;
    function loadArticles() {
      console.log('Load more articles...');
      params = {
        start: $('.Blog-New').length,
      };
      if (TotalNews < params.start) {
        // Aquí agregar cargador
        $.post('/blog/more', params, function(response, textStatus, xhr) {
          _html = "";
         //console.log(response);
          //return;
          news = response.news;
          for(x in news) {
            //console.log(response[x]);
            _html += '<div class="large-6 small-12 Blog-New"><div class="Blog-Image"><img src="/'+news[x].thumb+'"></div><div class="Blog-Resume"><div class="Blog-Title">'+news[x].titulo+'</div><div class="Blog-Date">Fecha: '+news[x].fecha_creacion+'</div><p class="Blog-Description">'+news[x].titulo+'</p><a href="/blog/'+news[x].url+'-'+news[x].id+'" class="Btn-Default"><i class="icon-eye"></i><span>VER MÁS</span></a></div></div>';
          }
          //_html.appendTo('.Body-Content-Blog');
          $('.Body-Content-Blog').append(_html);
          TotalNews = response.total;
          // Aquí quitar cargador
        }, 'json');
        return false;
      }
      else {
        console.log('No more news');
      }
    }

    function init() {
      scrollTop( $('.Button-Top') );
      validateSubscribe('.Suscribe-Form');
      //loadArticles();
      menuResponsive( $('.Menu-Sandwich') );
      console.log('terms');

      $('.Btn-Load').click(function(event) {
        event.preventDefault();
        loadArticles();
      });
    }

    $doc.ready(init);
});
