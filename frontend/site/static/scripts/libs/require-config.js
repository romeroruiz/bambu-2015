require.config({
  baseUrl:'/static/scripts',
  paths: {
    'fb': 'modules/fb',
    'tw': 'modules/tw',
    'async': 'libs/async',
    'autotab' : 'libs/jquery.autotab',
    'common': 'modules/common',
    'alphanumeric': 'libs/jquery.alphanumeric',
    //'register-form': 'modules/register-form',
    'floated-label': 'libs/floated.label',
    'custom-input': 'modules/custom-input',
    'validate': 'libs/jquery.validate/jquery.validate',
    'additional-methods': 'libs/jquery.validate/additional-methods',
    'validate-subscribe': 'modules/validate-subscribe',
    'validate-contact': 'modules/validate-contact',
    'scrolltop': 'modules/scroll-top',
    'menu-responsive': 'modules/menu-responsive',
    'slick': 'libs/slick',
    'ddslick': 'libs/ddslick/jquery.ddslick',
    'fancybox': 'libs/jquery.fancybox',
    'jquery': 'libs/jquery-1.11.0',
    'goog': 'libs/goog',
    'facebook': '//connect.facebook.net/es_LA/all',
    'twitter': '//platform.twitter.com/widgets'
  },
  shim: {
    'validate' : [
      'jquery'
    ],
    'additional-methods' : [
      'jquery'
    ],
    'floated-label' : [
      'jquery'
    ],
    'scrolltop' : [
      'jquery'
    ],
    'menu-responsive' : [
      'jquery'
    ],
    'validate-subscribe' : [
      'validate',
      'jquery'
    ],
    'validate-contact' : [
      'validate',
      'jquery'
    ],
    'custom-input' : [
      'jquery'
    ],
    'autotab' : [
      'jquery'
    ],
    'fancybox' : [
      'jquery'
    ],
    'facebook' : {
      exports: 'FB'
    },
    'twitter' : {
      exports: 'twttr'
    },
    'slick' : [
      'jquery'
    ],
    'ddslick' : [
      'jquery'
    ]
  }
});