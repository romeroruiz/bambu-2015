module.exports = function(grunt) {

  var config = grunt.config.get('config');

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.config.set('copy', {
    images: {
      files: [{ 
        expand: true,
        cwd: 'static/images',
        src: ['*','**/*'],
        dest: config.deploy_routes().images
      }]
    },
    fonts: {
      files: [{ 
        expand: true,
        cwd: 'static/fonts',
        src: ['*','**/*'],
        dest: config.deploy_routes().fonts
      }]
    }
  });

  if (grunt.option('prod')) {
    grunt.registerTask('deploy', 'Deployando en proyecto en produccion', function() {
      grunt.task.run([
        'templates',
        'styles',
        'cssmin:minify',
        'sprites',
        'copy',
        'requirejs',
        'removelogging:remove'
      ]);
    }); 
  } else {
    grunt.registerTask('deploy', 'Deployando el proyecto en desarrollo', function() {
      grunt.task.run(['templates', 'styles','sprites']);
    }); 
  }
  
};
