module.exports = function(grunt) {
  
  var config  = grunt.config.get('config'),
      dest    = config.deploy_routes().sprites;

  grunt.loadNpmTasks('grunt-glue');

  grunt.registerTask('sprites', 'Compile sprites', function() {
    var items = grunt.file.expand({}, [
          'static/sprites/*', '!static/sprites/**.*'
        ]),
        i = items.length - 1,
        sprites = {};

    for (i; i > -1; --i) {
      sprites['sprite' + i] = {
        src: [ items[i] ],
        options: '--css=' + dest +
          ' --namespace= '+
          '--sprite-namespace= '+
          ' --img=' + dest +
          ' --cachebuster --url=../sprites/ --margin=10'
      };
    }
    grunt.config.set('glue', sprites);

    grunt.config.set('stylus.glue', {
      options: {
        '-C': true
      },
      files: [
        {
          expand: true,
          cwd: dest,
          src: [
            '*.css'
          ],
          dest: 'static/styles/modules',
          ext: '.sprite.styl'
        }
      ]
    });

    grunt.task.run('glue');
    grunt.task.run('stylus:glue');
  });
};
