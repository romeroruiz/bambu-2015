module.exports = function(grunt) {
  
  var config = grunt.config.get('config'),
      dest   = config.deploy_routes().scripts;

  grunt.loadNpmTasks('grunt-remove-logging');

  grunt.config.set('removelogging', {
    remove: {
      files: [
        {
          cwd: dest,
          expand: true,
          src: ['*.js'],
          dest: dest
        }
      ]
    }
  });
};

