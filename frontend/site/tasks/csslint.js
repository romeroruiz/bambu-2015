module.exports = function(grunt) {
  
  var config = grunt.config.get('config'),
      dest = config.deploy_routes().styles;

  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.config.set('csslint', {
    lint: {
      options: {
        import: false
      },
      files:[{
        src: dest + '**/*.css'
      }]
    }
  });
}
