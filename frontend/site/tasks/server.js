var connect = require('connect'),
    grunt;

function staticServer(host, port) {

  var done = this.async(),
      dirname = '',
      config = grunt.config.get('config');

  host = host || 'localhost';
  port = port || 8080;
  dest = config.deploy_routes().base;
  con = connect();

  con.use(config.static_uri() + '/scripts', connect.static('static/scripts'));
  con.use(config.static_uri() + '/styles', connect.static(config.deploy_routes().styles));
  con.use(config.static_uri(), connect.static('static'));
  
  if (config.getEnv() !== 'prod') {
    con.use('/', connect.static(dest + '/templates'));
    con.use('/static/sprites/', connect.static(dest + '/static/sprites'));
  }

  con.listen(port, host);
  grunt.log.write('\nStarting static web server in "%s" on port %s.', host, port);
}

module.exports = function(g) {
  
  grunt = g;

  grunt.registerTask('connect', 'Start a custom static webserver', staticServer);

};