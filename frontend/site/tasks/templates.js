module.exports = function(grunt) {

  var local_config = grunt.config.get('config'),
      dest,
      ext;

  grunt.loadNpmTasks('grunt-contrib-jade');
  dest = config.deploy_routes().templates;
  ext = config.template_ext();
  grunt.config.set('jade', {
    compile: {
      options: {
        pretty: false,
        data: {
          config : local_config
        }
      },
      files: [
        {
          expand: true,
          cwd: 'templates/sections',

          src: [
            '*.jade',
            '**/*.jade',
            '!_layout.jade',
            '!**/_layout.jade',
            '!includes/**/*.jade',
            '!mixins/**/*.jade',
            '!_*.jade'
          ],
          dest: dest,
          ext: ext
        }
      ]
    }
  });

  grunt.registerTask('templates', 'Compiling Templates', function () {
    if (grunt.option('format')) {
      grunt.config.set('jade.compile.options.pretty', true);
    }

    if (!grunt.option('prod')) {
      grunt.config.set('jade.compile.files.0.ext', local_config.template_ext());
    }
    
    grunt.task.run(['jade']);
  });
};

