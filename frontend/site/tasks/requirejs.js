  module.exports = function(grunt) {

  var config    = grunt.config.get('config'),
      site      = grunt.config.get('site'),
      templates = config.templates,
      dest      = config.deploy_routes().scripts;

  grunt.loadNpmTasks('grunt-contrib-requirejs');

  grunt.config.set('requirejs', {
    deploy: {
      options: {
        appDir:'static/scripts',
        mainConfigFile: "static/scripts/libs/require-config.js",
        baseUrl: '.',
        dir: dest,
        //optimize: 'none',
        preserveLicenseComments: false,
        modules: [
          { name: 'site.home' },
          { name: 'site.blog' },
          { name: 'site.builds' },
          { name: 'site.zone' },
          { name: 'site.contact' }
        ]
      }
    }
  });
};