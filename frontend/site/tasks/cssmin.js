module.exports = function(grunt) {

  var config  = grunt.config.get('config'),
      dest    = config.deploy_routes().styles;

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.config.set('cssmin',{
    minify: {
      files: [
        {
          expand: false,
          cwd: dest,
          src: ['*.css'],
          dest: dest,
          keepSpecialComments:false
        }
      ]
    }
  });
}